import React from 'react';
import ReactDOM from 'react-dom';
import TodoAppHeader from './components/todo-app-header';
import TodoAppFooter from './components/todo-app-footer';
import TodoList from './components/todo-list';
import './style.scss';

const ENTER_KEY = 13;

class TodoApp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tasks: [],
      filter: 'all',
      taskText: '',
    };

    this.handleNewTaskTextChange = this.handleNewTaskTextChange.bind(this);
    this.handleNewTaskTextSubmit = this.handleNewTaskTextSubmit.bind(this);
    this.handleClearTask = this.handleClearTask.bind(this);
    this.handleToggleAllTask = this.handleToggleAllTask.bind(this);
    this.handleToggleTask = this.handleToggleTask.bind(this);
    this.handleDeleteTask = this.handleDeleteTask.bind(this);
    this.handleSetFilter = this.handleSetFilter.bind(this);
    this.handleEditTask = this.handleEditTask.bind(this);
  }

  /**
   * Приводит все задачи в одно состояние
   * @param done
   */
  handleToggleAllTask(done) {
    this.setState((prevState) => ({
      tasks: prevState.tasks.map((task) => ({ ...task, done })),
    }));
  }

  /**
   * Добавляет новую задачу
   * @param e
   */
  handleNewTaskTextSubmit(e) {
    if (e.which === ENTER_KEY && e.target.value) {
      const newTask = {
        text: e.target.value.trim(),
        id: Date.now(),
        done: false,
      };

      this.setState((prevState) => ({
        tasks: prevState.tasks.concat(newTask),
        taskText: '',
      }));
    }
  }

  handleNewTaskTextChange(taskText) {
    this.setState({ taskText });
  }

  /**
   * Удаляет завершённые задачи
   */
  handleClearTask() {
    this.setState((prevState) => ({
      tasks: prevState.tasks.filter((task) => task.done === false),
    }));
  }

  /**
   * Меняет состояние задачи
   * @param id
   * @param done
   */
  handleToggleTask(id, done) {
    this.setState((prevState) => ({
      tasks: prevState.tasks.map((task) => task.id === id ? { ...task, done } : task),
    }));
  }

  /**
   * Удаляет задачу
   * @param id
   */
  handleDeleteTask(id) {
    this.setState((prevState) => ({
      tasks: prevState.tasks.filter((task) => task.id !== id),
    }));
  }

  /**
   * Применяет новое значение фильтра
   * @param filter
   */
  handleSetFilter(filter) {
    this.setState({ filter });
  }

  /**
   * Меняет текст задачи
   * @param id
   * @param text
   */
  handleEditTask(id, text) {
    this.setState((prevState) => ({
      tasks: prevState.tasks.map((task) => task.id === id ? { ...task, text } : task),
    }));
  }

  render() {
    return (
      <section className="todo-app">
        <TodoAppHeader tasks={this.state.tasks}
                       taskText={this.state.taskText}
                       onHandleNewTaskTextSubmit={this.handleNewTaskTextSubmit}
                       onHandleNewTaskTextChange={this.handleNewTaskTextChange}
                       onHandleToggleAllTask={this.handleToggleAllTask}/>
        <TodoList tasks={this.state.tasks}
                  filter={this.state.filter}
                  onHandleToggleTask={this.handleToggleTask}
                  onHandleDeleteTask={this.handleDeleteTask}
                  onHandleEditTask={this.handleEditTask}/>
        <TodoAppFooter tasks={this.state.tasks}
                       filter={this.state.filter}
                       onHandleClearTask={this.handleClearTask}
                       onHandleSetFilter={this.handleSetFilter}/>
      </section>
    );
  }
}

ReactDOM.render(<TodoApp/>, document.getElementById('app'));
