import React from 'react';

export default class TodoAppFooter extends React.Component {
  constructor(props) {
    super(props);
    this.handleSetFilter = this.handleSetFilter.bind(this);
  }

  handleSetFilter(e) {
    this.props.onHandleSetFilter(e.target.name);
  }

  render() {
    const tasks = this.props.tasks;

    //Есть ли хоть одна выполненная задача
    let hasDoneTask = false;

    //Количество активных задач
    let taskActiveCount = 0;

    //Варианты фильтрации
    const filterTask = [
      { name: 'all', value: 'Все' },
      { name: 'active', value: 'В работе' },
      { name: 'completed', value: 'Выполниные' },
    ];

    if (tasks.length === 0) {
      return null;
    }

    tasks.forEach((task) => {
      if (hasDoneTask === false && task.done === true) {
        hasDoneTask = true;
      }

      if (task.done === false) {
        taskActiveCount++;
      }
    });

    return (
      <footer className="footer">
        <div className="footer__count">Всего задач: {taskActiveCount}</div>
        <div className="filters">
          {filterTask.map((item) => {
            const active = item.name === this.props.filter ? ' filters__item_active' : '';

            return <input type="button"
                          key={item.name}
                          name={item.name}
                          value={item.value}
                          className={`button filters__item${active}`}
                          onClick={this.handleSetFilter}/>;
          })}
        </div>
        <input type="button"
               value="Удалить выполненные"
               className="button button_delete"
               onClick={this.props.onHandleClearTask}
               disabled={hasDoneTask === false}/>
      </footer>
    );
  }
}
