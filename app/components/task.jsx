import React from 'react';

const ESCAPE_KEY = 27;
const ENTER_KEY = 13;

export default class Task extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      editMode: false,
    };

    this.handleToggleTask = this.handleToggleTask.bind(this);
    this.handleDeleteTask = this.handleDeleteTask.bind(this);
    this.startEditTask = this.startEditTask.bind(this);
    this.stopEditTask = this.stopEditTask.bind(this);
    this.handleKeyDown = this.handleKeyDown.bind(this);
    this.handleBlur = this.handleBlur.bind(this);
  }

  handleToggleTask(e) {
    this.props.onHandleToggleTask(this.props.task.id, e.target.checked);
  }

  handleDeleteTask() {
    this.props.onHandleDeleteTask(this.props.task.id);
  }

  /**
   * Запускает режим редактирование задачи
   */
  startEditTask() {
    this.setState({ editMode: true });
  }

  /**
   * Выходит из режима редактирование
   * задачи и сохраняет новое значение
   * @param value
   */
  stopEditTask(value) {
    const id = this.props.task.id;

    if (value) {
      //Если у задачи есть текст, то обновляем его
      this.props.onHandleEditTask(id, value);
    } else {
      //В противном случае удаляем задачу
      this.props.onHandleDeleteTask(id);
    }

    this.setState({ editMode: false });
  }

  componentDidUpdate() {
    if (this.state.editMode === true) {
      this.editInput.focus();
      this.editInput.setSelectionRange(0, this.editInput.value.length);
    }
  }

  handleKeyDown(e) {
    if (e.which === ENTER_KEY) {
      this.stopEditTask(e.target.value);
    }

    if (e.which === ESCAPE_KEY) {
      this.editInput.value = this.props.task.text;
      this.stopEditTask(this.props.task.text);
    }
  }

  handleBlur(e) {
    this.stopEditTask(e.target.value.trim());
  }

  render() {
    const completedCss = this.props.task.done ? ' todo-list__description_completed' : '';
    const editModeCss = this.state.editMode ? ' todo-list__task_edit' : '';

    return (
      <div className={`todo-list__task${editModeCss}`}
           onDoubleClick={this.startEditTask}>
        <input type="checkbox"
               className="checkbox todo-list__toggle"
               checked={this.props.task.done}
               onChange={this.handleToggleTask}/>
        <label className={`todo-list__description${completedCss}`}>{this.props.task.text}</label>
        <input type="text"
               className="todo-list__edit"
               onBlur={this.handleBlur}
               onKeyDown={this.handleKeyDown}
               ref={(input) => { this.editInput = input; }}
               defaultValue={this.props.task.text}/>
        <input type="button"
               className="button button_delete todo-list__delete"
               value="Удалить"
               title="Удалить задачу"
               onClick={this.handleDeleteTask}/>
      </div>
    );
  }
}
