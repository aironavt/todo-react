import React from 'react';
import Task from './task';

export default class TodoList extends React.Component {
  constructor(props) {
    super(props);

    this.filterTask = this.filterTask.bind(this);
  }

  /**
   * Отбирает задачи согласно фильтра из подвала
   * @param task
   * @returns {boolean}
   */
  filterTask(task) {
    const type = this.props.filter;

    if (type === 'active' && task.done === true) {
      return false;
    }

    if (type === 'completed' && task.done === false) {
      return false;
    }

    return true;
  }

  render() {
    const tasks = this.props.tasks.filter(this.filterTask);
    const emptyText = {
      active: 'Нет невыполненных задач',
      completed: 'Нет ни одной выполненной задачи',
    };

    if (tasks.length === 0 && this.props.tasks.length > 0) {
      return (
        <section className="todo-list">
          <div className="todo-list__empty-text">{emptyText[this.props.filter]}</div>
        </section>
      );
    }

    return (
      <section className="todo-list">
        {tasks.map(task =>
          <Task key={task.id} task={task}
                onHandleToggleTask={this.props.onHandleToggleTask}
                onHandleDeleteTask={this.props.onHandleDeleteTask}
                onHandleEditTask={this.props.onHandleEditTask}/>
        )}
      </section>
    );
  }
}
