const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const plugins = [
  new webpack.DefinePlugin({
    'process.env': {
      'NODE_ENV': JSON.stringify(process.env.NODE_ENV),
    },
  }),
  new ExtractTextPlugin({
    filename: 'styles.css',
    disable: process.env.NODE_ENV === 'hot',
  }),
  new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /ru|en/),
];

// HMR only on development
if (process.env.NODE_ENV === 'hot') {
  plugins.push(
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin()
  );
}

module.exports = {

  entry: process.env.NODE_ENV === 'hot' ?
    ['react-hot-loader/patch', 'webpack-hot-middleware/client', './app'] : './app',

  output: {
    path: path.join(__dirname, 'public'),
    filename: 'bundle.js',
  },
  plugins: plugins,

  resolve: {
    modules: [
      path.join(__dirname, 'app'),
      'node_modules',
    ],
    alias: { shared: path.join(__dirname, 'shared') },
    extensions: ['.js', '.jsx']
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        query: {
          presets: ['es2015', 'react', 'stage-0'],
          'env': {
            'hot': {
              'plugins': ['react-hot-loader/babel']
            },
          },
          plugins: ['transform-runtime']
        }
      },
      {
        test: /\.s?css$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: ['css-loader', 'postcss-loader', 'resolve-url-loader', 'sass-loader']
        })
      },
      {
        test: /\.(png|jpg|ttf|otf|eot|svg|woff|woff2|ico)$/,
        use: [{
          loader: 'file-loader',
          options: { name: '[name].[ext]' },
        }]
      }
    ]
  },

  devtool: 'eval-source-map',
  watch: true,
};
